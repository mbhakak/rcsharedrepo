function BuildForge () as Dynamic

	Forge = {}
	
	Forge.f_coreScreenDirty = false
	Forge.f_coreScreenClearColor = &h00000000
	Forge.f_coreScreenActive = false
	Forge.f_coreScreen = invalid
	Forge.f_coreCompositor = invalid
	Forge.f_port = CreateObject("roMessagePort")
	Forge.f_imageCache = invalid
	Forge.f_textureManager = invalid
	Forge.f_spriteList = {}
	Forge.f_topZindex = 0
	Forge.dirty = false
	
	
	Forge.inputManager = invalid
	Forge.eventManager = invalid
	Forge.requestManager = invalid
	Forge.tweenManager = invalid
	Forge.fontManager = invalid
	
	Forge.f_videoPlayer = invalid
	Forge.f_shapeIDGen = 0
	Forge.f_textIDGen = 0
	Forge.f_imageIDGen = 0
	Forge.f_hollowIDGen = 0
	Forge.f_impressionIDGen = 0
	Forge.f_spriteIDGen = 0
	
	
	'================================================================
	' Screen Methods
	'================================================================
	Forge.createScreen = function (pDoubleBuffer = false as Boolean, pAlphaEnabled = false as Boolean, pBGColor = &h00000000 as Integer )
		m.disposeScreen()
		m.f_coreScreenClearColor = pBGColor
		m.f_coreScreen = CreateObject ("roScreen", pDoubleBuffer)
		m.f_coreScreen.SetMessagePort(m.f_port)
		m.f_coreScreen.SetAlphaEnable(pAlphaEnabled)
		m.f_coreCompositor = CreateObject ("roCompositor")
		m.f_coreCompositor.SetDrawTo(m.f_coreScreen, pBGColor)
	end function
	
	Forge.AddChild = function (pSprite as Dynamic) as Void
		refSprite = m.f_coreCompositor.NewSprite(0, 0, pSprite.f_region, m.f_topZindex)
		m.f_spriteList[pSprite.id] = pSprite
		pSprite.native = refSprite
		m.dirty = true
		m.f_topZindex = m.f_topZindex + 1
	end function

	Forge.AddChildAt = function (pSprite as Dynamic, pIndex as Integer) as Void
		for each id in m.f_spriteList
			sprite = m.f_spriteList[id]
			if (sprite.native <> invalid)
				sIndex = sprite.native.GetZ()  
				if (sIndex >= pIndex)
					sprite.native.SetZ(sIndex+1)
 				end if
			end if
		end for
		m.f_topZindex = m.f_topZindex + 1
		refSprite = m.f_coreCompositor.NewSprite(0, 0, pSprite.f_region, m.f_topZindex)
		m.f_spriteList[pSprite.id] = pSprite
		pSprite.native = refSprite
		m.dirty = true
	end function
	
	Forge.RemoveChild = function (pSprite as Dynamic) as Void
		if (pSprite <> invalid)
			if (pSprite.native <> invalid)
				rIndex = pSprite.native.GetZ()
				for each id in m.f_spriteList
					sprite = m.f_spriteList[id]
					if (sprite.native <> invalid)
						sIndex = sprite.native.GetZ()
						if (sIndex > rIndex)
							sIndex = sIndex - 1
							sprite.native.SetZ(sIndex)
						end if
					end if
				end for
				pSprite.native.Remove()
				pSprite.native = invalid
			end if
		end if
		m.f_spriteList.Delete(pSprite.id)
		m.f_topZindex = m.f_topZindex - 1
		m.dirty = true
	end function

	Forge.SwapChildren = function (pID1 as String, pID2 as String) as Void
		sprite1 = m.f_spriteList[pID1]
		if (sprite1 <> invalid)
			if (sprite1.native <> invalid)
				sprite2 = m.f_spriteList[pID2]
				if (sprite2 <> invalid)
					if (sprite2.native <> invalid)
						temp = sprite2.native.GetZ()
						sprite2.native.SetZ(sprite1.native.GetZ())
						sprite1.native.SetZ(temp)
					end if
				end if
			end if
		end if
	end function
	
	Forge.DisposeScreen = function ()
		if (m.f_coreScreenActive)
			'TODO: dispose and clear coreScreen and coreCompositor
		end if
	end function
	
	'================================================================
	' Forge Core Methods
	'================================================================
	Forge.Update = function () as Void
		msg = m.f_port.GetMessage()
		if (msg <> invalid)
			'if (type(msg) = "roVideoPlayerEvent")
            '
            '     ?"------------------------------"
            '     ?"is isPlaybackPosition: " ; msg.isPlaybackPosition() ; ", pos: " ; msg.GetIndex()
            '     ?"is isRequestFailed: " ; msg.isRequestFailed() ; ", msg: " ; msg.GetMessage()
            '     ?"is isStatusMessage: " ; msg.isStatusMessage() ; ", msg: " ; msg.GetMessage()
            '     ?"is isStreamStarted: " ; msg.isStreamStarted() ; ", msg: " ; msg.GetIndex()
            '     ?"is isFullResult: " ; msg.isFullResult()
            '     ?"is paritalResult: " ; msg.isPartialResult()
            '     ?"is isPaused() : " ; msg.isPaused()
            '     ?"is isResumed() : " ; msg.isResumed()
            '     ?"is isScreenClosed() : " ; msg.isScreenClosed()
            '     ?"is isStreamSegmentInfo(): " ; msg.GetMessage()
            '     
            'end if
	       if (m.requestManager <> invalid)
               m.requestManager.ReceiveEvent(m.requestManager, msg)
           end if
           
           if (m.f_textureManager <> invalid)
           		m.f_textureManager.ReceiveEvent(msg)
           end if
           
           if (m.inputManager <> invalid)
           		m.inputManager.ReceiveEvent(msg)
           end if
           
           if (m.f_videoPlayer <> invalid)
           		m.f_videoPlayer.f_ReceiveEvent(msg)
           	end if
           
       end if
       m.tweenManager.Update() 	
	end function
	
	Forge.Draw = function () as Void
		if (m.dirty)
			m.f_coreScreen.Clear(m.f_coreScreenClearColor)
			m.f_coreCompositor.draw()
    		m.f_coreScreen.swapbuffers()
    		'ForgeLog("Forge","Drew Sprite")
    		m.dirty = false
    	end if
    end function
	
	'================================================================
	' Forge Sub Object Methods
	'================================================================
	Forge.GenerateID = function (pType as String) as String
		value = m["f_"+pType+"IDGen"]
		id = pType+"_"+value.toStr()
		if (value = 10000)
			value = -1
		end if
		m["f_"+pType+"IDGen"] = value+1
		return (id)
	end function
	
	'================================================================
	' Forge Private Methods
	'================================================================
	Forge.f_Initialize = function() as Void
		m.CreateScreen(true, true, &h00000000)
		m.eventManager = ForgeEventManager()
		m.inputManager = ForgeInputManager(m.eventManager)
		m.requestManager = ForgeRequestManager(m, m.f_port) 
		m.tweenManager = ForgeTweenManager()
		m.fontManager = ForgeFontManager()
		m.f_imageCache = F_ForgeImageCacheManager(m.requestManager)
		m.f_textureManager = F_ForgeAsyncImageManager(m.f_port)
		ForgeLog("Forge", "Initialized")
	end function

	
	'================================================================
	' Calls
	'================================================================
	Forge.f_Initialize()
	
	return (Forge)
	
end function