function Util_md5(text as String) as String
'    DebugLog("GeneralUtils", "util_md5")
    ba = CreateObject("roByteArray")
    ba.FromAsciiString(text)
    
    digest = CreateObject("roEVPDigest")
    digest.setup("md5")
    digest.Update(ba)
    
    result = digest.Final()
    
    return result
end function

'Function Util_AnyToString(any As Dynamic) As dynamic
'    if any = invalid return "invalid"
'    if isstr(any) return any
'    if isint(any) return itostr(any)
'    if isbool(any)
'        if any = true return "true"
'        return "false"
'    end if
'    if isfloat(any) return Str(any)
'    if type(any) = "roTimespan" return itostr(any.TotalMilliseconds()) + "ms"
'    return invalid
'End Function

function ConvertHexToRGB(hexColorValue as String, opacity as String) as Integer
 '   DebugLog("GeneralUtils", "convertHexToRGB")
    if hexColorValue = invalid then return invalid
    if opacity = invalid then opacity = "FF"

    'convert #FFFFFF to FFFFFF
    if left(hexColorValue, 1) = "#"
        hexColorValue = strReplace(hexColorValue, "#", "")
    end if 
   
    return HexToInteger(hexColorValue + opacity)

end function