Function ForgeVideoPlayer () as Dynamic

		'http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8 <http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8>

		this = {}
		this.f_forge = GetGlobalAA().forge
		this.f_forge.f_videoPlayer = this
		this.f_videoLoadingProgress = 0
    	this.f_clock = CreateObject("roDateTime")
    	this.f_clock.mark()
		
		this.isLive = true
		this.player = CreateObject("roVideoPlayer")
		
		this.playerArea     = { x: 0, y:0, w:0, h: 0 }
		
		this.player.SetMessagePort(this.f_forge.f_port)
	    this.player.SetDestinationRect(this.playerArea)
    	this.player.SetLoop(true)
    	this.player.SetPositionNotificationPeriod(1)
    	
    	this.firstLoad = true
    	
    	this.loadVideo = function()
    	end function
    	
    	
    	
    	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    	' Private
    	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    	this.f_ReceiveEvent = function (pMsg as Object) as Void
	  		 if (pMsg <> invalid)
             	if (type(pMsg) = "roVideoPlayerEvent")
            		if pMsg.isStatusMessage() and pMsg.GetMessage() = "startup progress"
                		progress% = pMsg.GetIndex() / 10
                		ForgeLog("ForgeVideoPlayer", "PercentLoaded: "+progress%.toStr())
                      	' display some sort of loading progress bar
                	end if
	            else if (msg.isPlaybackPosition())
               		'currentPlaybackPos = msg.GetIndex()
                end if
            end if
    	end function
    	
    	this.f_initialize = function () as Void
    		ForgeLog("ForgeVideoPlayer", "Initialized")
    	    bitrates = [0]
    		urls = ["http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8"]
            qualities = ["SD"]
            streamformat = "hls"
            title = "Apple BipBop Test Stream"
            srt=""
            
            videoclip = CreateObject("roAssociativeArray")
            videoclip.StreamBitrates = bitrates
            videoclip.StreamUrls = urls
            videoclip.StreamQualities = qualities
            videoclip.StreamFormat = StreamFormat
            videoclip.Title = title
            
             if srt <> invalid and srt <> "" then
                videoclip.SubtitleUrl = srt
             end if
            
             targetrect = { x: 0, y: 0, w: 1280, h: 720 }
            
             m.player.SetDestinationRect(targetrect)
             m.player.AddContent(videoclip)
             m.player.Play()
             m.f_forge.dirty = true
    	end function
    	
    	this.f_initialize()
    	
    	return this
    			

end Function