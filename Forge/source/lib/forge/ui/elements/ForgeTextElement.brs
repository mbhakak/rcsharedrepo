Function ForgeTextElement (pArgs = invalid as Object) as Object
	this = {}
	this.type = "TEXT"
	this.id = GetGlobalAA().forge.GenerateID("TEXT")

	this.name = ""
	this.source= "UNDEFINED"
	this.fontFamily = "Gotham HTF Condensed"
	this.fontSize = 18
	this.fontColor = "FFFFFF"
	this.alpha = 100
	this.x = 0
	this.y = 0
	this.index = 0
	this.native = invalid
	this.bitmap = invalid
	this.mask = invalid
	this.f_region = invalid
	
	for each arg in pArgs
		this[arg] = pArgs[arg]
	end for
	
	if (this.font = invalid)
		this.font = GetGlobalAA().forge.fontManager.GetFont(this.fontFamily, this.fontSize, false, false)
	end if
	
	if(this.width = invalid)          
        this.width = this.font.GetOneLineWidth(this.source, 1280)
        this.width = this.width + 10
    end if
    
    if(this.height = invalid)          
        this.height = this.font.GetOneLineHeight()
        this.height = this.height + 10
    end if
    
    temp = CreateObject("roByteArray")
	temp.push( CINT(this.alpha/100*255) )
	this.hexAlpha = temp.toHexString()
	if (this.alpha <> 100)
		this.alphaEnabled = true
	end if
	
    this.color = convertHexToRGB(this.fontColor, this.hexAlpha)
    
    this.Extrude = function () as Void
    	m.bitmap = CreateObject("roBitmap", {x:m.x, y:m.y, width:m.width, height:m.height})
		m.value = m.bitmap.DrawText(m.source, m.x, m.y, m.color, m.font)
		if (m.mask <> invalid)
			m.f_region = CreateObject("roRegion", m.bitmap, m.mask.x, m.mask.y, m.mask.width, m.mask.height)
		else
			m.f_region = CreateObject("roRegion", m.bitmap, 0, 0, m.bitmap.GetWidth(), m.bitmap.GetHeight())
		end if
    end Function
    
    
    this.Dispose = function () as Void
    	m.font = invalid
		if (m.native <> invalid)
			m.native.Remove()
			m.native = invalid
		end if
		m.mask = invalid
		m.bitmap = invalid
		m.f_region = invalid
    end function
	
	return this
end Function