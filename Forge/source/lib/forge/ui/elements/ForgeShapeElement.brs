Function ForgeShapeElement (pArgs = invalid as Object) as Object
	this = {}
	this.type = "SHAPE"
	this.id = GetGlobalAA().forge.GenerateID("SHAPE")
	
	' Defaults
	this.x = 0
	this.y = 0
	this.width = 0
	this.height = 0
	this.alpha = 100
	this.alphaEnabled = false
	this.color = "FFFFFF"
	this.index = 0
	this.native = invalid
	this.bitmap = invalid
	this.mask = invalid
	this.f_region = invalid
	
	' Populate this object with arguments
	if (pArgs <> invalid)
    	for each prop in pArgs
    		this[prop] = pArgs[prop]
    	end for
    end if
	
	temp = CreateObject("roByteArray")
	temp.push( CINT(this.alpha/100*255) )
	this.hexAlpha = temp.toHexString()
	
	if (this.alpha <> 100)
		this.alphaEnabled = true
	end if
	this.HexColor = ConvertHexToRGB(this.color, this.hexAlpha) 'Refer to ForgeUtil for function definition


	this.Extrude = function () as Void
		m.bitmap = CreateObject("roBitmap", {x:m.x, y:m.y, width:m.width, height:m.height})
		m.bitmap.drawRect(m.x, m.y, m.width, m.height, m.hexcolor)
		if (m.mask <> invalid)
			m.f_region = CreateObject("roRegion", m.bitmap, m.mask.x, m.mask.y, m.mask.width, m.mask.height)
		else
			m.f_region = CreateObject("roRegion", m.bitmap, 0, 0, m.bitmap.GetWidth(), m.bitmap.GetHeight())
		end if
	end function
	
	
	this.Dispose = function () as Void
		if (m.native <> invalid)
			m.native.Remove()
			m.native = invalid
		end if
		m.mask = invalid
		m.bitmap = invalid
		m.f_region = invalid
	end function
	
	
	return this
end Function