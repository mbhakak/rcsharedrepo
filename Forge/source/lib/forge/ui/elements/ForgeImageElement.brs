Function ForgeImageElement (pArgs as Object) as Object

	this = {}
	this.f_forge = GetGlobalAA().forge
	this.type = "IMAGE"
	this.id = this.f_forge.GenerateID("IMAGE")
	
	this.name = ""
	this.context = invalid
	this.imageURL = invalid
	this.getCached = true
	this.loaded = false
	this.OnLoadCallback = invalid
	this.OnFailCallback = invalid
	this.mask = invalid
	this.bitmap = invalid
	this.native = invalid

	this.x = 0
	this.y = 0
	this.width = 0
	this.height = 0
	this.scaleX = 0
	this.scaleY = 0
	this.index = 0
	
	this.f_region = invalid
	
	for each prop in pArgs
		this[prop] = pArgs[prop]
	end for
	
	
	'=============================================================================
	' Public Methods
	'=============================================================================
	this.Dispose = function () as Void
	 	if (m.bitmap <> invalid) then m.bitmap = invalid
     	if (m.region <> invalid) then m.region = invalid 
     	if (m.OnLoadCallback <> invalid) then m.OnLoadCallback = invalid
     	if (m.OnFailCallback <> invalid) then m.OnFailCallback = invalid
     	if (m.native <> invalid)
			m.native.Remove()
		end if
     	m.f_forge = invalid
     end function
 	
 	this.UpdateScale = function () as Void
 		if (m.bitmap <> invalid)
 			m.scaleX = m.width/m.bitmap.GetWidth()
 			m.scaleY = m.height/m.bitmap.GetHeight() 
 		end if
 	end function
 	
 	this.Extrude = function () as Void
 		if (m.bitmap <> invalid)
 			if (m.mask <> invalid)
				m.f_region = CreateObject("roRegion", m.bitmap, m.mask.x, m.mask.y, m.mask.width, m.mask.height)
			else
				m.f_region = CreateObject("roRegion", m.bitmap, 0, 0, m.bitmap.GetWidth(), m.bitmap.GetHeight())
			end if
		end if
 	end function
 	
 	this.Dispose = function() as Void
 		mask = invalid
 		m.bitmap = invalid
 		m.f_region = invalid
 		if (m.native <> invalid)
 			m.native.Remove()
 			m.native = invalid
 		end if
 	end function
 	
	'=============================================================================
	' Private Methods
	'=============================================================================
	this.f_initialize = function()
		if (m.getCached)
			m.f_forge.f_imageCache.GetImage(m, m.imageURL, m.f_OnImageLoadSuccess, m.f_OnImageLoadFail)
		else
			m.f_forge.f_textureManager.GetImage(m, m.imageURL, m.f_OnImageLoadSuccess, m.f_OnImageLoadFail)
		end if
	end function
	
	this.f_OnImageLoadSuccess = function (pBitmap as Object, pContext as Dynamic) as Void
		pContext.bitmap = pBitmap
		pContext.width = pContext.bitmap.GetWidth()
		pContext.height = pContext.bitmap.GetHeight()
		pContext.scaleX = 1
		pContext.scaleY = 1
		if (pContext.OnLoadCallback <> invalid)
			pContext.loaded = false
			pContext.OnLoadCallback(pContext.id, pContext.context)
		end if
	end function
	
	this.f_OnImageLoadFail = function (pBitmap as Object, pContext  as Dynamic) as Void
		pContext.bitmap = pBitmap
		if (pBitmap = invalid)
			pContext.bitmap = CreateObject ("roBitmap")
		end if
		pContext.width = 0
		pContext.height = 0
		pContext.scaleX = 0
		pContext.scaleY = 0
		if (pContext.OnFailCallback <> invalid)
			pContext.loaded = false
			pContext.OnFailCallback(pContext.id, pContext.context)
		end if
	end function
	
	'=============================================================================
	' Calls
	'=============================================================================
	this.f_Initialize()
	
	return (this)
end function