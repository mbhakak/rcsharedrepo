Function ForgeSprite (pArgs = invalid as Object) as Dynamic
	this = {}
	this.f_forge = GetGlobalAA().forge
	this.id = this.f_forge.GenerateID("SPRITE")
	
	this.native = invalid
	this.visible = true
	
	this.width = 0
	this.height = 0
	this.x = 0
	this.y = 0
	this.alpha = 100

	this.f_impressionList = {}
	this.f_prevWidth = 0
	this.f_prevHeight = 0
	this.f_bitmap = invalid
	this.f_region = invalid
		
	this.AddImpression = function (pImpression as Dynamic) as Void
		refWidth = CINT(Abs(pImpression.x) + pImpression.width)
		refHeight = CINT(Abs(pImpression.x) + pImpression.height)
		if (m.width < refWidth)
			m.f_prevWidth = m.width 
			m.width = refWidth
		end if
		if (m.height < refHeight) 
			m.f_prevHeight = m.height
			m.height = refHeight
		end if
		m.f_ImpressionList[pImpression.id] = pImpression
	end function
	
	this.RemoveImpression = function (pID as String) as Void
		impression = m.f_ImpressionList[pID]
		refWidth = CINT(Abs(impression.x) + impression.width)
		refHeight = CINT(Abs(impression.x) + impression.height)
		if (m.width = refWidth)
			m.width = m.f_prevWidth 
		end if
		if (m.height = refHeight) 
			m.height = m.f_prevHeight
		end if
		m.f_impressionList.Delete(pID)
	end function
	
	this.Extrude = function () as Void
			m.f_bitmap = CreateObject("roBitmap", {width:m.width, height:m.height, AlphaEnable:false})
			for each id in m.f_impressionList
				impression = m.f_impressionList[id]
				ForgeLog("ForgeSprite", "Smithing: "+impression.id)
				m.f_bitmap.DrawScaledObject(impression.x, impression.y, 1, 1, impression.bitmap)
			end for
			m.f_region = CreateObject("roRegion", m.f_bitmap, 0, 0, m.f_bitmap.GetWidth(), m.f_bitmap.GetHeight())
	end function
	
	this.Dispose = function () as Void
		m.f_impressionList.Clear()
		m.f_bitmap = invalid
		m.f_region = invalid
	end function
	
	'============================================================================
	' Private Methods
	'============================================================================
	this.f_Initialize = function ()
	end function
	
	this.f_MarkDirty = function()
		m.f_forge.dirty = true
	end function
	
	return (this)
end function