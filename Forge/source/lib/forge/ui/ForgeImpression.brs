Function ForgeImpression (pArgs = invalid as Object) as Dynamic
	this = {}
	this.type = "IMPRESSION"
	this.id = GetGlobalAA().forge.GenerateID("IMPRESSION")
	this.bitmap = invalid
	this.native = invalid
	this.mask = invalid
	this.f_region = invalid
	this.visible = true
	this.x = 0
	this.y = 0
	this.width = 0
	this.height = 0
	
	if (pArgs <> invalid)
    	for each prop in pArgs
    		this[prop] = pArgs[prop]
    	end for
   	end if
	
	
	this.f_elementList = []
	this.f_prevWidth = 0
	this.f_prevHeight = 0
	
	
	this.AddElement = function (pElement as Object) as Void
		count = m.f_elementList.Count()
		m.f_elementList.Push(pElement)
		pElement.index = count
		m.f_UpdateDimensionsOnAdd(pElement)
	end function
	
	this.AddElementAt = function (pElement as Object, pIndex as Integer) as Void
		count = m.f_elementList.Count()
		if (pIndex >= count)
			this.AddElement (pElement)
		else
			flipArray = []
			count = m.f_elementList.Count() -1
			for i = 0 to count
				currElement = m.f_elementList[i]
				if (currElement.index < pIndex)
					flipArray.Push(currElement)
				else if (pIndex = currElement.index)
					pElement.index = i
					flipArray.Push(pElement)
					currElement.index = currElement.index+1
					flipArray.Push(currElement)
				else 
					currElement.index = currElement.index+1
					flipArray.push(currElement)
				end if
			end for
			m.f_elementList = flipArray
			m.f_UpdateDimensionsOnAdd(pElement)
		end if
	end function
	
	this.RemoveElement = function (pElement as Object) as Void
		m.f_UpdateDimensionOnRemove(pElement)
		m.f_RemoveElementByIndex(pElement.index)
	end function
	
	this.RemoveElementByID = function (pID as String) as Void
		count = m.f_elementList.Count()-1
		for i = 0 to count
			if (m.f_elementList[i].id = pID)
				m.RemoveElement(m.f_elementList[i])
				exit for
			end if
		end for
	end function 
	
	this.RemoveElementByIndex = function (pIndex as Integer) as Void
		m.f_elementList.Delete(pIndex)
		count = m.f_elementList.Count()-1
		
		if (pIndex <= count)
			for i = pIndex to count
				m.f_elementList[i].index = i
			end for
		end if
	end function
	
	this.Extrude = function () as Void
		m.bitmap = CreateObject("roBitmap", {x:m.x, y:m.y, width:m.width, height:m.height})
		count = m.f_elementList.Count()-1
		for i = 0 to count
			element = m.f_elementList[i]
			if (element.type = "SHAPE")
				m.bitmap.drawRect(element.x, element.y, element.width, element.height, element.hexcolor)
			else if (element.type = "IMAGE")
				element.UpdateScale()
				m.bitmap.DrawScaledObject(element.x, element.y, element.scaleX, element.scaleY, element.bitmap)
			else if (element.type = "TEXT")
				m.bitmap.DrawText(element.source, element.x, element.y, element.color, element.font)
			end if
		end for
		if (m.mask <> invalid)
			m.f_region = CreateObject("roRegion", m.bitmap, m.mask.x, m.mask.y, m.mask.width, m.mask.height)
		else
			m.f_region = CreateObject("roRegion", m.bitmap, 0, 0, m.bitmap.GetWidth(), m.bitmap.GetHeight())
		end if
	end function
	
	this.Dispose = function () as Void
		m.f_elementList.Clear()
		m.bitmap = invalid
		m.f_region = invalid
		if (m.native <> invalid)
			m.native.Remove()
			m.native = invalid
		end if
	end function


	'============================================================================
	' Private Methods
	'============================================================================
	this.f_UpdateDimensionsOnAdd = function (pElement as Object) as Void
		refWidth = CINT(Abs(pElement.x) + pElement.width)
		refHeight = CINT(Abs(pElement.x) + pElement.height)
		if (m.width < refWidth)
			m.f_prevWidth = m.width 
			m.width = refWidth
		end if
		if (m.height < refHeight) 
			m.f_prevHeight = m.height
			m.height = refHeight
		end if
	end function 
	
	this.f_UpdateDimensionsOnRemove = function (pElement as Object) as Void
		element = m.f_elementList[pIndex]
		refWidth = CINT(Abs(element.x) + element.width)
		refHeight = CINT(Abs(element.x) + element.height)
		if (m.width = refWidth)
			m.width = m.f_prevWidth 
		end if
		if (m.height = refHeight) 
			m.height = m.f_prevHeight
		end if
	end function 
	
	this.f_Initialize = function ()
	end function

	'============================================================================
	' Calls
	'============================================================================
	this.f_Initialize()

	return (this)
end function