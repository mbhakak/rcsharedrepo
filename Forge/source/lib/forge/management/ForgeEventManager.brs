function ForgeEventManager() as Dynamic

		this = {}
		this.f_regCount = 0
		this.f_dispatchList = []
		
		this.addEventListener = function (pEventName as String, pCallbackFunc as Function, pContext as Dynamic) as Void
			m.f_regCount = m.f_regCount+1
			m.f_dispatchList.push({eName:pEventName, callback:pCallbackFunc, context:pContext})
		end function 
		
		this.removeEventListener = function (pEventName as String, pCallbackFunc  as Function, pContext as Dynamic) as Void
			refObj = invalid
			for i = 0 to m.f_regCount-1 step 1
				if pEventName = m.f_dispatchList[i].eName
					if (pCallbackFunc = m.f_dispatchList[i].callback)
						if (pContext = m.f_dispatchList[i].context)
    						m.f_dispatchList[i].eName = invalid
    						m.f_dispatchList[i].callback = invalid
    						m.f_dispatchList[i].context = invalid
    						m.f_dispatchList.delete(i)
    						m.f_regCount = m.f_regCount - 1					
    						exit for
    					end if
					end if
				end if
			end for
		end function
		
		this.dispatch = function (pEventName as String, pData as Object) as Void
			for i = 0 to m.f_regCount-1 step 1
				if (m.f_dispatchList[i].eName = pEventName)
					m.f_dispatchList[i].callback(pData, m.f_dispatchList[i].context)
				end if
			end for  
		end function
		
		return this
		
end function