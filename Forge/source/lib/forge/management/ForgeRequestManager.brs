function ForgeRequestManager (pForge as Dynamic, pPort as Object) as Object    
    this = {}
    this.f_forge = pForge
    this.f_port = pPort
    this.f_asyncList = {}
    
    this.CreateRequest = function(pParams)
        requestWrapper = {}
        
        ' When url param is incomplete
        if pParams.url = invalid or pParams.method = invalid
            requestWrapper = invalid
            return requestWrapper
        end if
        
        ' Builds a roURLTransfer object
        request = m.f_BuildRequestObject(pParams)
        
        ' Create a request wrapper
        requestWrapper["request"] = request
        requestWrapper["params"] = pParams
        requestWrapper["url"] = pParams.url
        requestWrapper["method"] = pParams.method
        return requestWrapper
    end function
    
    
    ' Fires the http request to the server in sync or async mode
    ' @Async mode : Makes the http request to the server and adds to the request manager stack
    ' @Sunc mode : Makes request to the server
    this.DispatchRequest = function(pRequestWrapper)
        if pRequestWrapper <> invalid
            request = pRequestWrapper.request
            requestType = pRequestWrapper.method
            requestParams = pRequestWrapper.params
            
            if requestParams.async = invalid or requestParams.async
                m.f_FireRequest(requestType, request, requestParams)
                m.f_asyncList[request.GetIdentity().toStr()] = pRequestWrapper
            else
                ' Make the call to be sync
                return m.f_FireSyncRequest(requestType, request, requestParams)
            end if
        end if
    end function
    
    
    ' Cancels all async request and clears the stack
    this.ClearRequestManager = function()
        for each requestWrapper in m.f_asyncList
            request = requestWrapper.request
            request.AsyncCancel()          
        end for
        
        m.f_requestManager.Clear()
    end function
    
    
    ' Receives all http related events and executes the callback
    ' @param : contains the app setting param
    this.ReceiveEvent = function(pRequestManager, pMsg)
        if type(pMsg) = "roUrlEvent"
            code = pMsg.GetResponseCode()
            id = pMsg.GetSourceIdentity().toStr()
            
            if pRequestManager.f_asyncList[id] <> invalid
                requestWrapper = pRequestManager.f_asyncList[id]    
                requestParams = requestWrapper.params
                
                if code >= 200 and code < 400
                    ' call success callback
                    requestParams.onSuccess(pMsg.GetString(), requestParams.context)
                    pRequestManager.f_asyncList.Delete(id)
                else 
                    ' Execute error callback
                    ' Handle error handling or re-request data
                    requestParams.onError(pMsg.GetFailureReason(), requestParams.context)
                    pRequestManager.f_asyncList.Delete(id)
                end if
            end if
        end if
    end function
    
    
    ' Helper Methods
    
    ' Builds out request params based on settings
    ' @params: object
    ' @return: returns a roUrlTransfer object
    this.f_BuildRequestObject = function(pParams)
    '    DebugLog("HttpRequestMaker", "BuildRequestObject")
        request = CreateObject("roUrlTransfer")
        request.SetMessagePort(m.f_port)
        request.RetainBodyOnError(true)
        request.SetUrl(pParams.url)
        
        ' Set Secure params
        if (pParams.IsSecure <> invalid and pParams.IsSecure)
            request.SetCertificatesFile("common:/certs/ca-bundle.crt")
            request.InitClientCertificates()
        end if
        
        ' Add request headers if set
        if(pParams.headers <> invalid)
            request.SetHeaders(pParams.headers)
        end if
        
        ' Set Data value
        if(pParams.data = invalid)
            pParams.data = ""
        end if
        
        ' Set this only for special requests
        if pParams.method = "DELETE" or pParams.method = "PUT"
            request.SetRequest(pParams.method)
        end if
        
        return request
        
    end function
    
    ' Fires the request to the server
    this.f_FireRequest = function(pRequestType, pRequest, pRequestParams)
        if pRequestType = "POST" or pRequestType = "PUT"
            pRequest.AsyncPostFromString(pRequestParams.data)
        else if (pRequestType = "FILE")
            pRequest.AsyncGetToFile(pRequestParams.filePath)
        else 
            pRequest.AsyncGetToString()
        end if
    end function
    
    ' Fires sync requests
    this.f_FireSyncRequest = function(pRequestType, pRequest, pRequestParams)
         if pRequestType = "POST" or pRequestType = "PUT"
            return pRequest.PostFromString(pRequestParams.data)
        else
            ' This needs to be set only for Delete and other special requests. Can cause unexpected results when always use
            return pRequest.GetToString()
        end if
        
    end function
    
    return this
    
end function