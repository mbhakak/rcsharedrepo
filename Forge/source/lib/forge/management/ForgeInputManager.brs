function ForgeInputManager (pEventManager as Object) as Dynamic

	this = {}
	this.eventManager = pEventManager
	
	this.ReceiveEvent = function (pMsg) as Void
		if (pMsg <> invalid)
			if (type(pMsg) = "roUniversalControlEvent")
    			if (pMsg < 100)
    				m.eventManager.dispatch("RemoteButtonPress", pMsg)
    			else 
    				m.eventManager.dispatch("RemoteButtonRelease", pMsg)
    			end if
    		end if
		end if
	end function
	
	
	return this
end function

' back = 0
' up = 2
' down = 3
' left = 4
' right = 5
'select = 6
'instant replay = 7
'rewind = 8
'fast forward = 9
'info = 10
'play = 13 