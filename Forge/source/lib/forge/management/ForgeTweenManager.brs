Function ForgeTweenManager ()

	this = {}
	this.f_clock = invalid
	this.f_prevTime = 0
	this.f_currTime = 0 
	this.f_delta = this.f_currTime - this.f_prevTime
	
	this.f_tweenIDCount = 0
	this.f_tweenCount = 0
	this.f_tweenList = {}
	this.f_easeFuncList = {}
	
	'==========================================================================
	' Private Easing Methods
	'==========================================================================
	this.f_EaseQuadInOut = function (pT, pB, pC, pD) as float
		t = pT / (pD/2)
		if (t < 1) 
			return (pC/2*t*t + pB)
		end if
		t = t - 1
		return ((-1*pC)/2 * (t*(t-2) - 1) + pB)
	end function
	
	this.f_EaseQuadIn = function (pT, pB, pC, pD) as float
		t = pT/pD
		return (pC*t*t + pB)
	end function
	
	this.f_EaseQuadOut = function (pT, pB, pC, pD) as float
		t = pT/pD
		return ((-1*pC) *t*(t-2)+pB)
	end function
	
	this.f_Initialize = function() as Void
		m.f_easeFuncList.QuadInOut = m.f_EaseQuadInOut
		m.f_easeFuncList.QuadIn = m.f_EaseQuadIn
		m.f_easeFuncList.QuadOut = m.f_EaseQuadOut
	end function
	
	'==========================================================================
	' Private Methods
	'==========================================================================
	this.f_UpdateDelta = function () as Void
		m.f_prevTime = m.f_currTime
		m.f_currTime = m.f_clock.TotalMilliseconds()
		m.f_delta = m.f_currTime - m.f_prevTime
	end function
	
	this.f_GetEasingFunction = function (pFuncName as String) as Dynamic
		if (pFuncName = invalid)
			pFuncName = "QuadInOut"
		end if
		return (m.f_easeFuncList[pFuncName])
	end function
	
	this.f_DeleteTween = function (pTween as Object) as Void
		pTween.obj = invalid
		pTween.context = invalid
		pTween.startProps = invalid
		pTween.endProps = invalid
		m.f_tweenManager.f_tweenList.Delete(pTween.id)
		m.f_tweenManager.f_tweenCount = m.f_tweenManager.f_tweenCount-1
		if (m.f_tweenManager.f_tweenCount = 0)
			m.f_tweenManager.f_clock = invalid
		end if
	end function
	
	this.f_UpdateTweenIDCount = function() as Void
		m.f_tweenIDCount = m.f_tweenIDCount+1
		if (m.f_tweenIDCount > 1000)
			m.f_tweenIDCount = 0
		end if
	end function
	
	'==========================================================================
	' Calls
	'==========================================================================
	this.f_Initialize()
	
	
	'==========================================================================
	' Public Methods
	'==========================================================================
	this.Tween = function(pContext as Dynamic, pObj as Object, pDuration as Integer, pProps = invalid as Object, pEase = invalid as Object, pComplete = invalid, pUpdate = invalid) as Object
		tween = {}
		tween.context = pContext
		tween.id = "ID_"+m.f_tweenIDCount.toStr()
		tween.obj = pObj
		tween.startProps = {}
		ForgeLog("TweenManager", "Start Set")
		for each prop in pProps
			tween.startProps[prop] = pObj[prop]
    		ForgeLog("TweenManager", tween.startProps[prop])
		end for
		ForgeLog("TweenManager", "End Set")
		tween.endProps = pProps
		tween.duration = pDuration
		tween.OnComplete = pComplete
		tween.OnUpdate = pUpdate
		tween.Ease = m.f_GetEasingFunction(pEase)
		tween.DeleteTween = m.f_DeleteTween
		tween.deltaCount = 0
		tween.f_tweenManager = m
		
		tween.Update = function (pDelta as Integer) as Void
			m.deltaCount = m.deltaCount + pDelta
			if (m.obj <> invalid)
    			for each prop in m.startProps
    				ForgeLog("TweenManager", m.deltaCount)
    				m.obj[prop] = m.Ease(m.deltaCount, m.startProps[prop], (m.endProps[prop]-m.startProps[prop]), m.duration)
    				ForgeLog("TweenManager", m.obj[prop])
    			end for
    			if (m.obj.f_MarkDirty <> invalid)
    				m.obj.f_MarkDirty()
    			end if
    			if (m.OnUpdate <> invalid)
    				m.OnUpdate(m.context)
    			end if
    		end if
    		
			if (m.deltaCount >= m.duration)
				if (m.OnComplete <> invalid)
					m.OnComplete(m.context)
				end if
				m.DeleteTween(m)
			end if
		end function
		
		if (m.f_tweenCount = 0)
			m.f_clock = CreateObject ("roTimeSpan")
			m.f_clock.Mark()
			m.f_currTime = m.f_clock.TotalMilliseconds()
			m.f_prevTime = m.f_currTime
		end if 
		m.f_UpdateTweenIDCount()
		m.f_tweenList[tween.id] = tween
		m.f_tweenCount = m.f_tweenCount + 1
		return (tween)
	end function
	
	this.Update = function () as Void 
		if (m.f_tweenCount > 0)
    		m.f_UpdateDelta()
    		for each tween in m.f_tweenList
    			m.f_tweenList[tween].Update(m.f_delta)
    		end for
    	end if
	end function
	
	return this

end Function