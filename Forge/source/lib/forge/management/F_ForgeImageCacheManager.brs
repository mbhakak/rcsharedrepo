function F_ForgeImageCacheManager(pRequestManager as Dynamic) as Dynamic
    
    this = {}
    this.f_requestManager = pRequestManager
    this.f_fs = CreateObject( "roFileSystem" )
 
    
    this.GetImage = function(pContext as dynamic, pImageUrl as String, pSuccessCallBack, pErrorCallback, pPlaceHolderImage=invalid) as Void
        this = m
        imgBitmap = invalid
        
        pContext.OnSuccess = pSuccessCallBack
        pContext.OnError = pErrorCallback
        pContext.placeHolderImage = pPlaceHolderImage
        
        if (Left(pImageUrl, 3) = "pkg") 
            imgBitmap = CreateObject("roBitmap", pImageUrl) 
            pSuccessCallBack(imgBitmap, pContext)
        else
            pContext.filePath = "tmp:/" + util_md5(pImageUrl)
            
            If Not this.f_fs.Exists(pContext.filePath) 
                requestData = {
                    this        : this
                    context     : pContext,
                    url         : pImageUrl,
                    isSecure    : false,
                    method      : "FILE",
                    filePath    : pContext.filePath
                    async       : true,
                    onSuccess   : this.f_ImageCacheSuccess
                    onError     : this.f_ImageCacheFail
                }
                request = this.f_requestManager.CreateRequest(requestData)
                this.f_requestManager.DispatchRequest(request)
            else
                imgBitmap = CreateObject("roBitmap", pContext.filePath) 
                pSuccessCallBack(imgBitmap, pContext)
            end if
        end if
    end function
    
    this.f_ImageCacheSuccess = function(pMsg as String, pContext as Dynamic)
        imageCacheContext = m.this
        controlContext = pContext
        imgBitmap = CreateObject("roBitmap", controlContext.filepath)
        if (imgBitmap = invalid)
            if (pContext.OnError <> invalid) then pContext.OnError("Image download failed", controlContext)
        else
            if (pContext.OnSuccess <> invalid) then pContext.OnSuccess(imgBitmap, controlContext)
        end if
    end function
    
    this.f_ImageCacheFail = function(pMsg as String, pContext as Dynamic)
        imageCacheContext = m.this
        controlContext = pContext
        if (pContext.onError <> invalid) then pContext.OnError("Image download failed", controlContext)
    end function
    
    return this

end function