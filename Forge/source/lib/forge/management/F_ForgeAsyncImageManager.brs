' Manages activities such as firing and receiving async texture calls
function F_ForgeAsyncImageManager(pPort as Object) as Object

    this = {}
    this.f_port = pPort
    this.f_mgr = CreateObject("roTextureManager")
    this.f_mgr.SetMessagePort(this.f_port)
    this.f_requestList = { }
    
    
    this.GetImage = function (pContext as dynamic, pImageURL as String, pSuccessCallback, pErrorCallback)
    	ticket = {}
    	ticket.request = CreateObject("roTextureRequest", pImageURL)
    	ticket.context = pContext
    	ticket.OnSuccess = pSuccessCallback
    	ticket.OnFail = pErrorCallback
    	ticket.id = ticket.request.GetId().toStr()
    	m.f_requestList[ticket.id] = ticket
    	m.f_mgr.RequestTexture(ticket.request)
    end function
    
    ' Receives all events related to texture manager
    this.ReceiveEvent = function(pMsg) as Object
        if pMsg <> invalid and type(pMsg) = "roTextureRequestEvent"
            state = pMsg.GetState()
            id = pMsg.GetId()
            uri = pMsg.GetURI()
            
            if state = 3
                if (m.f_requestList[id.toStr()] <> invalid)
                    ticket = m.f_requestList[id.toStr()]
                    if ticket.OnSuccess <> invalid and ticket.OnFail <> invalid
                    	bitmap = pMsg.GetBitmap()
                    	if (type(bitmap) <> "roBitmap")
                    		ticket.OnFail(bitmap, ticket.context)
                    	else
                    		ticket.OnSuccess(bitmap, ticket.context)
                    	end if
                    end if
                    m.f_requestList.Delete(id.toStr())
                    m.f_mgr.UnloadBitmap(uri)
                    ticket.OnSuccess = invalid
                    ticket.OnFail = invalid
                    ticket.context = invalid
                    ticket.request = invalid
                end if
            end if
        end if
    end function

    return this
    
end function