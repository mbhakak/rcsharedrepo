Function Block()

    this = {}
    
    this.x = 0
    this.y = 0
    this.title = ""
    this.isLive = false
    this.type = -1
    this.periodIndicator = ""
    this.showtime = ""
    this.relatedCoverage = -1
   
    this.backgroundElement = invalid
    this.titleElement = invalid
    this.timeElement = invalid
    this.liveElement = invalid
    
    this.impression = invalid
    
    
    this.setBlock = function(px as integer, py as integer, ptitle as string, pisLive as boolean, ptype as integer, ptime as string, pRC = -1 as integer)
        m.x = px
        m.y = py
        m.title = ptitle
        m.isLive = pislive
        m.type = ptype
        
        if (m.type = 0)         'show
            m.showtime = ptime
            m.timeElement = ForgeTextElement({source:m.showtime, x:20, y:100, fontFamily:"Gotham HTF Condensed", fontSize:30, fontColor:"FFFFFF", alpha:100})
        else if (m.type = 1)    'segment
            m.periodIndicator = ptime
            m.timeElement = ForgeTextElement({source:m.periodIndicator, x:20, y:100, fontFamily:"Gotham HTF Condensed", fontSize:30, fontColor:"FFFFFF", alpha:100})
        else if (m.type = 2)    'related coverage
            m.showtime = ""
            m.periodIndicator = ""
        end if
    
        m.backgroundElement = ForgeShapeElement({x:0, y:0, width:340, height:120, color:"333333", alpha:100})
        m.titleElement = ForgeTextElement({source:m.title, x:50, y:45, fontFamily:"Gotham HTF Condensed", fontSize:35, fontColor:"FFFFFF", alpha:100})
        if (m.isLive)
            m.liveElement = ForgeTextElement({source:"LIVE NOW", x:250, y:10, fontFamily:"Gotham HTF Condensed", fontSize:25, fontColor:"FF0000", alpha:100})
        end if
        
        m.impression = ForgeImpression()
        m.impression.x = m.x
        m.impression.y = m.y
        m.impression.AddElement(m.backgroundElement)
        m.impression.AddElement(m.titleElement)
        if (m.liveElement <> invalid)
            m.impression.AddElement(m.liveElement)
        end if    
        
        m.impression.Mold()
        
    end function            
    
    this.deleteBlock = function()
    end function
    
    
    return this

end function