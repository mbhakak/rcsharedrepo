function Swimlane()

    this = {}
    
    this.id = ""
    this.x = 0
    this.y = 0
    this.width = 0
    this.height = 0
    m.color = ""
    
    this.backgroundElement = invalid
    
    this.blocks = []
    this.impression = invalid
    
    ' functions
    
    this.setSwimlane = function(pid as string, px as integer, py as integer, pwidth as integer, pheight as integer, pcolor as string)
    
        m.id = pid
        m.x = px
        m.y = py
        m.width = pwidth
        m.height = pheight
        m.color = pcolor
        
        m.backgroundElement = ForgeShapeElement({x:0, y:0, width:1280, height:180, color:m.color, alpha:100})     
        m.impression = ForgeImpression()
        m.impression.x = m.x
        m.impression.y = m.y
        m.impression.AddElement(m.backgroundElement)   
        
        m.impression.Mold()
        
    end function
    
    this.loadBlock = function(pblock as Object)
        m.blocks.push(pblock)
    end function

    return this

end function