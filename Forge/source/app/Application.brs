function Application ()
	
	this = {}
	
	this.forge = GetGlobalAA().forge

	this.currView = invalid
	this.currViewID = 0
	
	this.f_OnViewComplete = function (pData as Object, pContext as Dynamic) as Void
		' create the next view off screen
		' transition the current view out
		'transition the next view in
		'dispose the current view
		'set the current view to be the next view
	end function
	
	this.initialize = function ()  as Void
		m.forge.eventManager.addEventListener("ViewComplete", m.f_OnViewComplete, m)
		m.currView = SampleView()
	end function
	
	this.initialize()
	
	
	return this
end function