Function SampleView()
	this = {}
	this.forge = GetGlobalAA().forge
	
	this.f_TweenUpdate = function(pContext as Dynamic)
		pContext.laneSprite.native.moveTo(pContext.laneSprite.x, pContext.laneSprite.y)
        'pContext.spriteBlock.native.moveOffset(pContext.spriteBlock.x, pContext.spriteBlock.y)
        'pContext.forge.dirty = true
    end function
    
    
    this.f_TweenComplete = function(pContext as Dynamic)
    	ForgeLog("SampleView", "Tween Complete")
    end function
    
	
	this.f_inputHandler = function (pKeyCode as Integer, pContext as Dynamic) as Void
        if (pKeyCode = 5)
           ForgeLog("SampleView", pContext.laneSprite.x)
           ForgeLog("SampleView", pContext.laneSprite.x+100)
           pContext.forge.tweenManager.tween(pContext, pContext.laneSprite, 500, {x:pContext.laneSprite.x + 100}, "QuadInOut", pContext.f_TweenComplete, pContext.f_TweenUpdate)
        else if (pKeyCode = 4)
           pContext.forge.tweenManager.tween(pContext, pContext.laneSprite, 500, {x:pContext.laneSprite.x - 100}, "QuadInOut",  pContext.f_TweenComplete, pContext.f_TweenUpdate)
        end if
	end function
	
	
	
'this.f_initialize = function()
'	m.laneSprite = ForgeSprite()
'	
'	for i=0 to 2
'		bgElement = ForgeShapeElement({x:0, y:0, width:340, height:120, color:"333333", alpha:100})
'		bgElement.Extrude()
'		m.forge.AddChild(bgElement)
'		timeElement = ForgeTextElement({source:"5 Pm", x:20, y:80, fontFamily:"Gotham HTF Condensed", fontSize:30, fontColor:"FFFFFF", alpha:100})
'		titleElement = ForgeTextElement({source:"Some Title", x:20, y:10, fontFamily:"Gotham HTF Condensed", fontSize:30, fontColor:"FFFFFF", alpha:100})
'		
'		impression = ForgeImpression()
'		impression.x = i*360
'		impression.AddElement(bgElement)
'		impression.AddElement(timeElement)
'		impression.AddElement(titleElement)
'		impression.Extrude()
'		
'		m.laneSprite.AddImpression(impression)
'	end for
'	
'	m.laneSprite.Extrude()
'	m.forge.AddChild(m.laneSprite)
'	m.laneSprite.x = 100
'	m.laneSprite.native.MoveTo(100, 0)
'	 m.forge.eventManager.addEventListener("RemoteButtonPress", m.f_inputHandler, m)
'end function 

	this.f_initialize = function ()
		timeElement = ForgeTextElement({source:"5 Pm", x:0, y:0, fontFamily:"Gotham HTF Condensed", fontSize:30, fontColor:"FFFFFF", alpha:100})
		timeElement.Extrude()
		m.forge.AddChild(timeElement)
	end function
	
	this.f_initialize()
	
	return (this)
end Function