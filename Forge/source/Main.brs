Library "v30/bslCore.brs"    'Import the bslCore library, refer to the roku sdk documentation to see what the library entails


' application runner function
sub Main ()
	
	'Setup global variables
	globals = GetGlobalAA()
	globals.debugMode = true
	
	globals.forge = BuildForge()
	
	forge = globals.forge
	
	app = Application()
	
	while (true)
		forge.Update()
		forge.Draw()
	end while
	
end sub  ' End Main()